<html>
<head>
    @yield('style')
    <style>
      /* Set the size of the div element that contains the map */
      #map {
        height: 500px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
        background-image: url("imagenes/loading-animated-gif.gif");
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center; 
        border: 1px solid #000000;
       }
       .miSeleccion{
            background-color: #33cccc;
       }

</style>
</head>
<body>

<div class="container-fluid"> 
  
    @yield('nav')
    <div class="row m-0">
        <div class="col12 col-sm-12 col-md-12 col-lg-12 m-0 p-0">
            <div id="map">
                
                <input id="longitud" name="longitud" type="hidden" value="<?php echo $busqueda['longitud'] ?>">
                <input id="latitud" name="latitud" type="hidden" value="<?php echo $busqueda['latitud'] ?>">
                
            </div>
        </div>
    </div>
    
    @yield('buy')
    <div class="row miSeleccion m-0 p-2">
        <div class="col12 col-sm-12 col-md-12 col-lg-12 m-0 p-0 text-center">
                <div class="">
                <h3>Has buscado:</h3>
                <h4>Comunidad:</h4>
                @foreach ($comun as $com)
                    {{$com->comunidad}}
                @endforeach

                </div>
                <div class="">
                <h4>Provincia:</h4>
                @foreach ($provincia as $prov)
                    {{$prov->provincia}}
                @endforeach
                </div>
                <div class="">
                <h4>Municipio:</h4>
                @foreach ($municipio as $mun)
                    {{$mun->municipio}}
                @endforeach
                </div>
               
        </div>
    </div>
    <div class="row m-0">
        <div class="col12 col-sm-12 col-md-12 col-lg-12 m-0 p-0 text-center">
            <div class="list-group">
                            <h3>Selecciona tu centro comercial</h3>
                            
            </div>
        </div>
    </div>
    
    @yield('subfooter')
    @yield('footer')
    @yield('scripts')
    <script  language="javascript" src="{{asset('js/mostrarMapa.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9VkNJqr5d0zdYtrAd8j8iE2rjm_0doKc&libraries=places&callback=initMap" async defer></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="  crossorigin="anonymous"></script>
  
    

</div>
</body>
</html>