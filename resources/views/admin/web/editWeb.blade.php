@extends('admin.layout')
@section('links')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">
<!-- Select2 -->
<link rel="stylesheet" href="/adminlte/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

@endsection

@section('header')
<div class="col-sm-6">
    <h1 class="m-0 text-dark">Editar Web</h1>
</div><!-- /.col -->
<div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Inicio</a></li>
    </ol>
</div><!-- /.col -->

@endsection

@section('content')
<form method="GET" action="{{ route('admin.escrito.create') }}">
    <!-- {{ csrf_field() }} {{ method_field('PUT')}} -->
    <div class="row">
        <div class="col-md-8">
            <div class="card text-center">
                <div class="card-header">
                    <h3 class="card-title">Crear un nuevo texto tríptico</h3>

                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="form-control @error('title') is-invalid @enderror">Título del texto</label>
                        <input name="title" value="" class="form-control" placeholder="introduce el título del texto">
                        {!! $errors->first('title','<span class="help-block text-danger">:message</span>') !!}
                    </div>

                    <div class="form-group">

                        <label class="form-control @error('body') is-invalid @enderror">Contenido de la
                            texto</label>

                        <textarea class="textarea" name="body" placeholder="Introduce el contenido aquí"
                            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                        {!! $errors->first('body','<span class="help-block text-danger">:message</span>') !!}
                    </div>

                </div>
                <div class="form-group">
                        <label class="form-control @error('buttonText') is-invalid @enderror">Texto del botón</label>
                        <input name="button_title" value="boton" class="form-control" placeholder="introduce el título del botón">
                        {!! $errors->first('buttonText','<span class="help-block text-danger">:message</span>') !!}
                    </div>
                <div class="form-group">
                    <label class="form-control @error('seccion_id') is-invalid @enderror">Sección</label>
                    <select name="parte_id" class="form-control select2" style="width: 100%;">
                        <option value="-1">Selecciona una sección</option>                     
                        @foreach($partes as $parte)
                        <option value="{{$parte->id}}">{{$parte->name}}</option>
                        @endforeach                      
                    </select>
                    
                </div>
  
            </div>
            <div class="form-group">
                        <button type="submit" class="btn btn-primary">Guardar Texto</button>
                    </div>
</form>
@endsection