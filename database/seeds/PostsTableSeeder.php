<?php
use App\Post;
use Carbon\Carbon;
use App\Category;
use Illuminate\Database\Seeder;
use App\Tag;
use Illuminate\Support\Facades\Storage;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Storage::disk('public')->deleteDirectory('posts');
        Post::truncate();
        Category::truncate();
        Tag::truncate();

        $category = new Category;
        $category->name = "Categoria1";
        $category->save();

        $category = new Category;
        $category->name = "Categoria2";
        $category->save();
        

        $post = new Post;
        $post->title = "Mi primer post";
        $post->url = Str::slug("Mi primer post");
        $post->excerpt = "Mi primer post";
        $post->body="<p>Contenido de mi primer post</p>";
        $post->published_at = Carbon::now();
        $post->category_id = 1;
        $post->save();
        $post->tags()->attach(Tag::create(['name' => 'etiqueta1']));


        $post = new Post;
        $post->title = "Mi segundo post";
        $post->url = Str::slug("Mi segundo post");
        $post->excerpt = "Mi segundo post";
        $post->body="<p>Contenido de mi segundo post</p>";
        $post->published_at = Carbon::now();
        $post->category_id = 2;
        $post->save();
        $post->tags()->attach(Tag::create(['name' => 'etiqueta2']));

    }
}