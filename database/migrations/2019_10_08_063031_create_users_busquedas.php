<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersBusquedas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_busquedas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cbx_comunidad');
            $table->string('cbx_provincia');
            $table->string('cbx_municipio');
            $table->double('longitud',10, 6);
            $table->double('latitud',10, 6);
            $table->date('fecha');
            $table->time('hora');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_busquedas');
    }
}
