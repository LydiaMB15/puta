<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Parte;
use App\Escrito;

class EscritController extends Controller
{
    public function index()
    {
        $partes = Parte::all();
        return View('admin.web.editWeb',compact('partes'));
    }

    public function create(Escrito $escrito, Request $request)
    {
        $escrito = new Escrito();
        $escrito->title = $request->title;
        $escrito->body = $request->body;
        $escrito->button_title = $request->button_title;
        $escrito->parte_id = $request->parte_id;
        $escrito->save();

        $partes = Parte::all();
        return View('admin.web.editWeb',compact('partes'));

    }
}
