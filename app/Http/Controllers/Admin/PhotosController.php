<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Support\Facades\Storage;
use App\Photo;


class PhotosController extends Controller
{
    public function store(Post $post)
    {
        $this->validate(request(),[
            'photo', 'image|max:2048'
        ]);
       
        $post->photos()->create([      
            'url' => Storage::url(request()->file('photo')->store('posts','public')),
        ]);

    }

    public function destroy(Photo $photo)
    {
        
        $photo->delete(); //eliminamos registro de la bbdd

        $photoPath = str_replace('storage','', $photo->url); //procesamos url de la imagen
      
        Storage::disk('public')->delete($photoPath);
           
        return back()->with('flash','Foto eliminada');
    }
}
