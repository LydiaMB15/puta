<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Post;
use App\Category;
use App\Tag;
use Illuminate\Support\Str;
use App\Http\Requests\StorePostRequest;


class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view('admin.posts.index',compact('posts'));
    }

    // public function create()
    // {
    //     $categories = Category::all();
    //     $tags = Tag::all();
    //     return view('admin.posts.create',compact('categories','tags'));
    // }

    public function store(Request $request)
    {
        
        $this->validate($request, ['title' => 'required']);

        $post = new Post;
        $post->title = $request->title;  
        $post->url = Str::slug($request->title);  
        $post->save();

        return redirect()->route('admin.posts.edit', $post);
    }


    public function edit(Post $post)
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.posts.edit',compact('categories','tags','post'));
    }

    public function update(Post $post, StorePostRequest $request)
    {
        //var_dump($request->all());
        $post->update($request->all());

        $post->syncTags($request->get('tags'));

        return redirect()->route('admin.posts.edit', $post)->with('flash', 'La publicación ha sido guardada' );
       
    }

    public function destroy(Post $post)
    {
        $post->delete();

        $post->delete();

        return redirect()->route('admin.posts.index')->with('flash', 'La publicación ha sido eliminada' );
    }
    
}
