<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Marker;

class mapaProcesarController extends Controller
{
    //
    public function procesarPeticion(Request $request,$id)
    {
        
       
        //dd($id);
        //buscar id por session y mostrar
        $idGB = substr($id, 1, strlen($id)-2);
       // dd($idGB);
        $marker = new Marker();
        $marker = $marker::encontrarMarcadorSession($idGB);

        //habría que buscar todos los asesores relacionados con ese cc y mostralos
        //asi como sus precios x hora

        return view('procesar',['marker' => $marker]);
    }
    
}
