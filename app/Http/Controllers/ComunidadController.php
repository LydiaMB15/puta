<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comunidad;
use APP\Provincia;
use App\Escrito;
use App\Parte;

class ComunidadController extends Controller
{
    public function show()
    {
        return 'hola';
    }
    
    public function index(){
        $comunidades = Comunidad::all();
        $escritos = Escrito::all();
        $partes = Parte::all();
        return view('index',compact('comunidades','escritos','partes'));
     
    }
}
