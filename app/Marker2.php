<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marker2 extends Model
{
    protected $table = 'ultimos_markers_buscados';
    public $timestamps = false;

    public static function ultimaBusquedaUser()
    {
        return Marker2::latest()->first();
      
    }
}
