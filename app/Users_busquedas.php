<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_busquedas extends Model
{
    protected $table = 'users_busquedas';


     public static function ultimaBusquedaUser()
     {
         return Users_busquedas::latest()->first();
       
     }

}
