<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{

    protected $fillable = [     

        'title','body','iframe','excerpt','published_at','category_id',     
    ];

   // protected $guarded = [];
    protected $dates = ['published_at'];

    public function category() //$post->category->namespace
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);

    }
    
    public function scopePublished($query) //querybuilder//queryscope
    {
        $posts =  Post::latest('published_at')
                    ->where('published_at','<=',Carbon::now());
                  

    }
     /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'url';
    }

 
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }
  
    /*
    public function photos()
    {
        return $this->morphMany(Photo::class,'photoable');
    }
    */
    public function setPublishedAtAttribute($published_at)
    {
        $this->attributes['published_at'] = Carbon::parse($published_at);;
    }

    public function setCategoryIdAttribute($category)
    {
        $this->attributes['category_id'] = Category::find($category) 
                                                    ? $category
                                                    : Category::create(['name' => $category])->id;
    }

    public function syncTags($tags)
    {
        $tagIds = collect($tags)->map(function($tag){
            return Tag::find($tag)
                        ? $tag  
                        : Tag::create(['name' => $tag])->id;
        });

        return $this->tags()->sync($tagIds);

    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($post){
            $post->tags()->detach();
        
            foreach($post->photos as $photo)
            {
                $photo->delete();
            }
        });
    }
    


}