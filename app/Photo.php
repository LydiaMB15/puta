<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Photo extends Model
{
    protected $guarded = [];

     /**
    * Get the owning imageable model.
    */
    /*
    public function photoable()
    {
        return $this->morphTo();
    }
    */


    protected static function boot()
    {
        parent::boot();

        static::deleting(function($photo){
            $photoPath = str_replace('storage','', $photo->url); //procesamos url de la imagen
      
            Storage::disk('public')->delete($photoPath);
        });
    }
}
