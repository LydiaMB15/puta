<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parte extends Model
{
    public function escritos()
    {
        return $this->hasMany(Escrito::class);
    }
}
