<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comunidad extends Model
{
    protected $table = 'comunidades';

    public static function getComunidad($id)
    {
        return Comunidad::where('id', '=', $id)
                        ->get();
    }

}
