<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escrito extends Model
{
    public function parte()
    {
        return $this->belongsTo(Parte::class);
    }
    /*
    public function photos()
    {
        return $this->morphMany(Photo::class,'photoable');
    }
    */
}
