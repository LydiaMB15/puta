<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marker extends Model
{
    protected $table = 'markers';
    public $timestamps = false;

    public static function marcadoresSesion()
    {
        return Marker::where('session',session()->getId())->get();
      
    }

    public static function existeMarcadorSession($idG)
    {
        return Marker::where('session',session()->getId())
            ->where('idG',$idG)
            ->first();
    }

    public static function encontrarMarcadorSession($idG)
    {
        return Marker::where('session',session()->getId())
            ->where('idG','like',$idG)
            ->get();
    }
 

}
